import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="sharepointcollector",
    version="0.0.2",
    author="corlee",
    author_email="info@vantouronline.nl",
    description="Sharepoint REST API Wrapper",
    long_description=long_description,
    url="https://gitlab.com/corlee/sharepoint-collector",
    packages=setuptools.find_packages(include=['sharepointcollector']),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "office365-rest-client==2.2.0"
    ]
)
