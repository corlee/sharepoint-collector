from office365.runtime.auth.client_credential import ClientCredential
from office365.runtime.client_request_exception import ClientRequestException
from office365.sharepoint.client_context import ClientContext
from office365.sharepoint.files.file import File
from office365.sharepoint.files.file_collection import FileCollection


class Credentials(object):

    def __init__(self, client_id, client_secret):
        """
        Client credentials

        :type client_secret: str
        :type client_id: str
        """
        self.clientId = client_id
        self.clientSecret = client_secret


class SharepointCollector(Credentials):

    def __init__(self, credentials, base_url):
        self.base_url = base_url
        self.ctx = ClientContext(self.base_url).with_credentials(
            ClientCredential(credentials.clientId, credentials.clientSecret)
        )

    def get_files_in_folder(self, path) -> FileCollection:
        """
        Retrieve files in folder

        :param path:
        :return:
        """
        try:
            collection = self.ctx.web.get_folder_by_server_relative_url(path).files
            self.ctx.load(collection)
            self.ctx.execute_query()

            return collection
        except ClientRequestException as e:
            print(e)
        except ValueError as e:
            print('valueError', e)

    def get_contents(self, file: File):
        """
        Get file contents

        :param file:
        :return:
        """
        try:
            response = File.open_binary(self.ctx, file.get_property('ServerRelativeUrl'))
            return response.content
        except Exception as e:
            raise e

    def remove_file(self, file: File):
        """
        Remove file object
        :param file: File
        :return: void
        """
        self.ctx.web.get_file_by_server_relative_url(file.get_property('ServerRelativeUrl')).delete_object()
        self.ctx.execute_query()

